# ZAnarch

![](media/logo_big.png)

*make Anarch a little more bearable for the general public*

- [compiling](#compiling)
- [manifesto](#manifesto)
- [FAQ](#faq)
- [code guide](#code-guide)
- [usage rights](#usage-rights)

## compiling

Much like Anarch originally, Linux users can run **./make sdl** to compile the game for SDL2.
For Windows users, simply download Tiny C Compiler, insert its root directory (the stuff containing TCC.exe) into a folder named TCC, and run the .bat file.

## "manifesto"

In today's world of capitalism and reactionary software, nobody has time to focus on he finer details of how things can be convenient for the end user. Sure, things are definitely libre, accessible, and ethical, but are they user-friendly?

ZAnarch aims to solve this by rejecting the shitty part of Suckless that makes everything elitist by adding extra options into the game itself, so as to allow on-the-spot configuration and tinkering without the requirement of recompiling the game every time you want to rebind a single key or turn on/off view bobbing. Sure, this essentially removes compatibility with lowest-end devices like the Pokkito, but ZAnarch isn't focused on things like that; it's focused on PCs and PC-like devices like the Steam Deck or anything of at least decent capability to run it.

## FAQ

### Why?

Because Suckless is *(in terms of convenient configuraton for non-power users)* a fucking joke and I want to let more people experience the glory of Anarch's programming without much hassle.

### Is this a joke?

Somewhat.

### Why are you doing this? Anarch was perfectly fine on its own!

I agree, Anarch is perfectly fine for a suckless program and a game, even if I don't agree with some things about it or its philosophies. However, these disagreements include the argument of Anarch calling itself "made for the benefit of living beings" despite it being somewhat difficult for these aforementioned living beings, with basically no understanding of what a C programming language is, to be able to take benefit of this game.

TL;DR, people are retarded nowadays. Not my problem, not yours either.

### I can make this in "Unity" in a week.

That's not a question. Also, that would violate the very philosophy of minimalist, ethical software. It adds on unnecessary bloat and slaps on several restrictions, even including non-legal ones like the capability of portability. By making a game on Unity, you're sucking the dick of major computer platforms, as WELL as a corporation. Besides, Unity games run like shit, and even the most impressive ones look like shit when on low resolutions.

### Isn't this a violation of Anarch's original philosophies, especially its preference for lowest-spec computers.

Absolutely! ZAnarch isn't exactly made FOR lowest-specs computers, anyway; it's made for systems with a bit of persistent storage, and some not-shitty power supply and CPU Hz. It MAY run on a Pokitto, trust me.

### Peace.

Fuck you.

## code guide

Most things should be obvious and are documented in the source code itself. This
is just an extra helper. The source code structure is mostly untouched.

The repository structure is following:

```
assets/          asset sources (textures, sprites, maps, sounds, ...)
  *.py           scripts for converting assets to C structs/arrays
media/           media presenting the game (screenshots, logo, ...)
mods/            official mods
constants.h      game constants that aren't considered settings
game.h           main game logic
images.h         images (textures, sprites) from assets directory converted to C
levels.h         levels from assets directory converted to C
main_*.*         fronted implement. for various platforms, passed to compiler
palette.h        game 256 color palette
raycastlib.h     ray casting library
settings.h       game settings that users can change (FPS, resolution, ...)
smallinput.h     helper for some frontends and tests, not needed for the game
sounds.h         sounds from assets directory converted to C
texts.h          game texts
make.sh          compiling script constaining compiler settings
HTMLshell.html   HTML shell for emscripten (browser) version
index.html       game website
README.md        this readme
```

Most things from Anarch are kept intact to avoid touching the main gameplay of the game itself. Most ZAnarch modifications are **technological modifications**, to fully utilize the capability of the program's frontend.

Speaking of which, ZAnarch **relies on the frontend for drawing** rather than forcing the CPU to do it; the original rendering method is still present within the game, but by default, the game will instead let the frontend, like SDL2, handle all rendering. This way, there's higher potential performance on systems with other components like GPUs, a la most computers nowadays, because while Anarch fully uses the CPU for handling rendering, ZAnarch can leverage some or all of the rendering work to the GPU for extra efficiency.

ZAnarch plans for the potential use of drummyfish's **3D rendering engine** small3dlib for more precise 3D rendering, even despite the affine texture mapping limitation. It won't be enabled by default, for consideration of lower-end hardwares, and will be located within the options.

The use of **fixed-point integers** is still employed, both due to compatibility concerns and because it still presents leeway to let systems that can't natively handle floating point numbers.

**Game mods** are either in the original form of using diff files--the suckless way--or via toggles in the options menu, or through modification of the externalized game assets. There is little intention to add support for native floating point numbers.

**Sprite** (monster, items, ...) rendering is intentionally kept simple and doesn't always give completely correct result, but is good enough. Sprite scaling due to perspective should properly be done with respect to both horizontal and vertical FOV, but for simplicity the game scales them uniformly, which is mostly good enough. Visibility is also imperfect and achieved in two ways simultaneously. Firstly a 3D visibility ray is cast towards each active sprite from player's position to check if it is visible or not (ignoring the possibility of partial occlusion). Secondly a horizontal 1D z-buffer is used solely for sprites, to not overwrite closer sprites with further ones.

**Monster sprites** only have one facing direction: to the player. To keep things small, each monster has only a few frames of animations: usually 1 idle frame, 1 waling frame, 1 attacking frame. For dying animation a universal "dying" frame exists for all mosnters.

All ranged attacks in the game use **projectiles**, there is no hit scan.

The game uses a custom general purpose HSV-based 256 color **palette** which I again maintain as a separate project in a different repository as well (see it for more details). The advantage of the palette is the arrangement of colors that allows increasing/decreasing color value (brightness) by incrementing/decrementing the color index, which is used for dimming environment and sprites in the distance into shadow/fog without needing color mapping tables (which is what e.g. Doom did), saving memory and CPU cycles for memory access.

All **assets** are embedded directly in the source code and will be part of the compiled binary. This way we don't have to use file IO at all, and the game can run on platforms without a file system. Some assets use very simple compression to reduce the binary size. Provided python scripts can be used to convert assets from common formats to the game format.

All **images** in the game such as textures, sprites and backgrounds (with an exception of the font) are 32 x 32 pixels in 16 colors, i.e. 4 bits per pixel. The 16 color palette is specific to each image and is a subpalette of the main 256 color palette. The palette is stored before the image data, so each image takes 16 + (32 * 32) / 2 = 528 bytes. This makes images relatively small, working with them is easy and the code is faster than would be for arbitrary size images. One color (red) is used to indicate transparency.

The game uses a custom very simple 4x4 **font** consisting of only upper case letters and a few symbols, to reduce its size.

For **RNG** a very simple 8 bit congruent generator is used, with the period 256, yielding each possible 8 bit value exactly once in a period.

**AI** is very simple. Monsters update their state in fixed time ticks and move on a grid that has 4 x 4 points in one game square. Monsters don't have any direction, just 2D position (no height). Close range monsters move directly towards player and attack when close enough. Ranged monsters randomly choose to either shoot a projectile (depending on the aggressivity value of the monster's type) or move in random direction.

User/platform **settings** are also part of the source code, meaning that change of settings requires recompiling the game. To change settings, take a look at the default values in `settings.h` and override the ones you want by defining them before including `game.h` in your platform's front end source code.

To increase **performance**, you can adjust some settings, see settings.h and search for "performance". Many small performance tweaks exist. If you need a drastic improvement, you can set ray casting subsampling to 2 or 3, which will decrease the horizontal resolution of raycasting rendering by given factor, reducing the number of rays cast, while keeping the resolution of everything else (vertical, GUI, sprites, ...) the same. You can also divide the whole game resolution by setting the resolution scaledown, or even turn texturing completely off. You can gain or lose a huge amount of performance in your implementation of the `SFG_setPixel` function which is evaluated for every single pixel in every frame, so try to optimize here as much as possible. Also don't forget to use optimization flags of your compiler, they make the biggest difference. You can also try to disable music, set the horizontal resolution to power of 2 and similat things.

**Levels** are stored in levels.h as structs that are manually editable, but also use a little bit of compression principles to not take up too much space, as each level is 64 x 64 squares, with each square having a floor heigh, ceiling heigh, floor texture, ceiling texture plus special properties (door, elevator etc.). There is a python script that allows to create levels in image editors such as GIMP. A level consists of a tile dictionary, recordind up to 64 tile types, the map, being a 2D array of values that combine an index pointing to the tile dictionary plus the special properties (doors, elevator, ...), a list of up to 128 level elements (monsters, items, door locks, ...), and other special records (floor/ceiling color, wall textures used in the level, player start position, ...).

**Saving/loading** is an optional feature. If it is not present (frontend doesn't implement the API save/load functions), all levels are unlocked from the start and no state survives the game restart. If the feature is implemented, progress, settings and a saved position is preserved in permanent storage. What the permanent storage actually is depends on the front end implementation – it can be a file, EEPROM, a cookie etc., the game doesn't care. Only a few bytes are required to be saved. Saving of game position is primitive: position can only be saved at the start of a level, allowing to store only a few values such as health and ammo.

Performance and small size are achieved by multiple **optimization** techniques. Macros are used a lot to move computation from run time to compile time and also reduce the binary size. Powers of 2 are used whenever possible. Approximations such as taxicab distances are used. Some "accelerating" structures are also used, e.g. a 2D bit array for item collisions. Don't forget to compile the game with -O3, and to turn down settings like the 3-point renderer if lag ensues on your device.

## usage rights

**tl;dr: everything in this repository is CC0 + a waiver of all rights, completely public domain as much as humanly possible, do absolutely anything you want**

I, blitzdoughnuts, waive all modifications I have made to this repository originally by Miloslav Ciz (drummyfish) under the CC0 public license, with any and all rights to code modifications given to the public for any use.
